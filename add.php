<?php
require('components/header.php');
try {
    require('components/connect.php');
    ?>
    <section class="main__add">
        <div class="main__add-container">
            <form action="components/addnews.php" method="POST" enctype="multipart/form-data">
                <input type="text" placeholder="Название*" id="name" name="name" maxlength="125" required>
                <br>
                <p>Автор:
                    <select id="author" name="author">
                        <?php
                        $sql = "SELECT * FROM author";
                        $result = $conn->query($sql);
                            while ($row = $result->fetch(PDO::FETCH_ASSOC)) :?>
                                <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                            <?php endwhile;
                        ?>
                    </select>
                </p>
                <input type="text" placeholder="Анонc*" id="preview" name="preview" maxlength="125" required>
                <br>
                <textarea placeholder="Описание*" id="desc" name="desc" maxlength="5000" required></textarea>
                <br>
                <p>Картинка:<input type=file id="image" name="image"></p>
                <p class="main__add--text-like">Похожие материалы:</p>
                <br>
                <select multiple="multiple" size="10" name="likes[]" id="likes">
                    <?php
                    $sql1 = "SELECT * FROM news";
                    $result1 = $conn->query($sql1);
                        while ($row1 = $result1->fetch(PDO::FETCH_ASSOC)) :?>
                            <option value="<?= $row1['id'] ?>"><?= $row1['name'] ?></option>
                        <?php endwhile;
                    ?>
                </select>
                <br>
                <input type="submit" id="send" value="Добавить">
            </form>
        </div>
    </section>
    <?php
}
catch (PDOException $e) {
    echo "error" .$e->getMessage();
}
require('components/footer.php');
?>
