<?php
require('components/header.php');
try {
    require('components/connect.php');

    $id = $_GET['id'];

    $ip = $_SERVER['REMOTE_ADDR'];

    $sql3 = "INSERT INTO visitors VALUES (NULL, :ip_address, NOW(), :news_id) ";
    $new = $conn->prepare($sql3);
    $new->execute(['ip_address' => $ip, 'news_id' => $id]);

    $sql1 = "SELECT * FROM news WHERE id ='$id'";
    $result = $conn->query($sql1);

    while ($row = $result->fetch(PDO::FETCH_ASSOC)) :?>

        <section class="main__new">
            <div class="main__new-container">
                <div class="main__new--name">
                    <h1><?= $row["name"] ?></h1>
                </div>
                <div class="main__new--image">
                    <img src="<?= $row['image'] ?>">
                </div>
                <div class="main__new--description">
                    <p><?= $row["content"] ?></p>
                </div>
                <div class="main__new--information">
                    <div class="main__new--date">
                        <p>Дата публикации: <?= $row["date"] ?></p>
                    </div>
                    <div class="main__new--viewers">
                        <p>Просмотров: <?= $row["views"] ?></p>
                    </div>
                </div>
                <?
                $author = $row["author"];

                $sql5 = "SELECT * FROM author WHERE id='$author'";
                $result2 = $conn->query($sql5);
                while ($row3 = $result2->fetch(PDO::FETCH_ASSOC)) :?>
                    <p style="text-align: center">Автор статьи: <?= $row3['name'] ?></p>
                <?php endwhile; ?>
                <h2>Похожие новости:</h2>
                <ul class="main__new-like">
                    <?
                    $sql2 = "SELECT * FROM likes WHERE news_id='$id'";
                    $result0 = $conn->query($sql2);
                    while ($row0 = $result0->fetch(PDO::FETCH_ASSOC)) {
                        $likes_id = $row0['likes'];
                        $sql3 = "SELECT * FROM news WHERE id='$likes_id'";
                        $result1 = $conn->query($sql3);
                        while ($row1 = $result1->fetch(PDO::FETCH_ASSOC)) : ?>
                            <button>
                                <a href="/news/<?= $row1['id'] ?>" title="<?= $row1['preview'] ?>">
                                    <li class="main__new-like--card">
                                        <div class="main__new-like--image">
                                            <img src="<?= $row1['image'] ?>">
                                        </div>
                                        <div class="main__new-like--name">
                                            <p><?= $row1["name"] ?></p>
                                        </div>
                                    </li>
                                </a>
                            </button>
                        <?php endwhile;
                    } ?>
                </ul>
            </div>
        </section>
    <?php endwhile;

    $sql4 = "UPDATE news SET views=views+1 WHERE id='$id'";

    if ($conn->query($sql4) === TRUE) {
        // echo "recorded updated!";
    }
}
catch (PDOException $e) {
    echo "error" .$e->getMessage();
}

require('components/footer.php');
?>