<?php
try {
    require('connect.php');

    if (empty($_POST['name']))  exit("Поле не заполнено");
    if (empty($_POST['desc']))  exit("Поле не заполнено");
    if (empty($_POST['preview']))  exit("Поле не заполнено");
    if (empty($_POST['author']))  exit("Поле не заполнено");

    if(isset($_FILES['image'])) {
        $errors = array();
        $file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $file_tmp = $_FILES['image']['tmp_name'];
        $file_type = $_FILES['image']['type'];
        $file_ext = strtolower(end(explode('.',$_FILES['image']['name'] )));

        $expensions = array ("jpeg","jpg", "png");

        if ($file_size > 2097152) {
            $errors[] = "Файл должен быть 2 МБ";
        }

        if (empty($errors) == true) {
            move_uploaded_file($file_tmp, "images/".$file_name);
            $path ="/components/images/".$file_name;
        } else {
            print $errors;
        }
    }

    $like = $_POST['likes'];

    $query = "INSERT INTO news VALUES (NULL,  :name, :preview, :content, :path, NOW(), :author , '0')";
    $new = $conn->prepare($query);
    $new->execute(['name' => $_POST['name'], 'preview' => $_POST['preview'], 'content' => $_POST['desc'], 'path' => $path, 'author'=>$_POST['author']]);

    $last_id=$conn->lastInsertId();

    $query1 = "INSERT INTO likes VALUES (NULL, :news_id, :likes)";
    $new1 = $conn->prepare($query1);
    if($like) {
        foreach ($like as $lake) {
            $new1->execute(['news_id' => $last_id, 'likes' => $lake]);
        }
        foreach ($like as $lake) {
            $new1->execute(['news_id' => $lake, 'likes' => $last_id]); //Ранее заполненые новости также будут рекомендовать данную новость
        }
    }

    header("Location: ../");
}
catch (PDOException $e) {
    echo "error" .$e->getMessage();
}
