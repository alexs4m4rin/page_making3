<?php
try {
    require('components/connect.php');
    ?>
    <section class="main__slide">
        <div class="main__slide-news">
            <h1>Популярное</h1>
            <ul>
                <?php
                $sql = "SELECT * FROM news ORDER BY `views` DESC ";
                $result = $conn->query($sql);
                    while ($row = $result->fetch(PDO::FETCH_ASSOC)) :?>
                        <li>
                            <button>
                                <a href="http://everydaynews/news/<?= $row['id'] ?>" title="<?= $row['name'] ?>">
                                    <div class="main__slide-news--image"><img src="<?= $row['image'] ?>" width="140px">
                                    </div>
                                    <div class="main__slide-news--card">
                                        <div class="main__slide-news--name"><h3><?= $row["name"] ?></h3></div>
                                        <div class="main__slide-news--preview"><p><?= $row["preview"] ?></p></div>
                                    </div>
                                </a>
                            </button>
                        </li>
                    <?php endwhile;
                ?>
            </ul>

        </div>
    </section>
    <?php
}
catch (PDOException $e) {
    echo "error" .$e->getMessage();
}
?>